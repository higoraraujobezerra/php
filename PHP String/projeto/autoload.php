<?php

spl_autoload_register(function ($classe) {
    $prefixo = "App\\";

    $diretorio = __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
    $tamanhoPrefixo = strlen($prefixo);

    if (strncmp($prefixo, $classe, $tamanhoPrefixo) !== 0) {
        return;
    }

    $arquivo = $diretorio . str_replace('\\', DIRECTORY_SEPARATOR, substr($classe, $tamanhoPrefixo)) . '.php';

    if (file_exists($arquivo)) {
        require $arquivo;
    }
});
