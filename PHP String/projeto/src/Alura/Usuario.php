<?php

namespace App\Alura;

class Usuario
{
    private $nome;
    private $sobrenome;
    private $senha;
    private $tratamento;

    public function __construct(string $nome, string $senha, string $tratamento)
    {
        $nomeSobrenome = explode(" ", $nome, 2);

        $this->nome = $nomeSobrenome[0];
        $this->sobrenome = $nomeSobrenome[1];

        empty($this->nome) ? $this->nome = "Nome inválido" : "";
        is_null($this->sobrenome) ? $this->sobrenome = "Sobrenome inválido" : "";

        $this->senha = $senha;
        if (!$this->validaSenha($senha)) {
            $this->senha = 'Senha inválida';
        }

        $this->atribuiTratamentoAoNome($nome, $tratamento);
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function getTratamento()
    {
        return $this->tratamento;
    }

    private function validaSenha(string $senha)
    {
        return strlen(trim($senha)) >= 6;
    }

    private function atribuiTratamentoAoNome($nome, $tratamento)
    {
        if ($tratamento === 'M') {
            $this->tratamento = preg_replace('/^(\w+)\b/', 'Sr.', $nome, 1);
        }

        if ($tratamento === 'F') {
            $this->tratamento = preg_replace('/^(\w+)\b/', 'Sra.', $nome, 1);
        }
    }
}
