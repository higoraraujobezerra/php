<?php

namespace App\Alura;

class Contato
{
    private $email;
    private $endereco;
    private $cep;
    private $telefone;

    public function __construct(string $email, string $endereco, string $cep, string $telefone)
    {
        $this->email = $email;

        if (!$this->validaEmail($email)) {
            $this->email = "E-mail inválido";
        }

        $this->endereco = $endereco;
        $this->cep = $cep;
        $this->telefone = $telefone;

        if (!$this->validaTelefone($telefone)) {
            $this->telefone = 'Telefone inválido';
        }
    }

    public function getUsuario()
    {
        $posicaoArroba = strpos($this->email, '@');

        if ($posicaoArroba) {
            return substr($this->email, 0, $posicaoArroba);
        }

        return "E-mail inválido";
    }

    public function getEmail()
    {
        return $this->email;
    }

    private function validaEmail(string $email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function getEndecoCep()
    {
        return implode(' - ', [$this->endereco, $this->cep]);
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    private function validaTelefone($telefone)
    {
        return preg_match('/^[0-9]{4}-[0-9]{4}$/', $telefone);
    }
}
