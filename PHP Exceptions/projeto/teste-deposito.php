<?php

use Alura\Banco\Model\Conta\ContaCorrente;
use Alura\Banco\Model\Conta\Titular;
use Alura\Banco\Model\CPF;
use Alura\Banco\Model\Endereco;

require_once "autoload.php";

$contaCorrente = new ContaCorrente(
    new Titular(
        new CPF('123.456.789-10'), 
        'Higor', 
        new Endereco('Socarobaca', 'Éden', 'Rua', '43')
    )
);

try{
    $contaCorrente->depositar(-100);
} catch(InvalidArgumentException $exception){
    echo "Valor a depositar precisa ser positivo";
}




