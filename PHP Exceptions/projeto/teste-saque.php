<?php

require_once 'autoload.php';

use Alura\Banco\Model\Conta\{Conta, ContaPoupanca, ContaCorrente, Titular, SaldoInsuficienteException};
use Alura\Banco\Model\{CPF, Endereco};

$conta = new ContaPoupanca(
    new Titular(
        new CPF('406.360.178-18'),
        'Higor', 
        new Endereco(
            'Sorocaba', 
            'Éden', 
            'rua', 
            '43'
        )
    )
);

$conta->depositar(50);
echo $conta->recuperarSaldo() . PHP_EOL;


try{
    $conta->sacar(100);
} catch(SaldoInsuficienteException $exception){
    echo "Você não tem saldo para realizar este saque." . PHP_EOL;
    echo $exception->getMessage() . PHP_EOL;
}

echo $conta->recuperarSaldo() . PHP_EOL;

