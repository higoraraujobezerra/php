<?php

namespace Alura\Banco\Service;

use Alura\Banco\Model\Funcionario\Funcionario;

class ControladorDeBonificacao
{
    private $totalBonificacao = 0;

    public function adicionaBonificacao(Funcionario $funcionario)
    {
        $this->totalBonificacao += $funcionario->calculaBonificacao();
    }

    public function recuperarTotal() : float
    {
        return $this->totalBonificacao;
    }
}