<?php

namespace Alura\Banco\Model;

trait AcessoPropriedades
{
    public function __get(string $nomeAtributo) : string
    {
        
        $metodo = 'recuperar' . ucfirst($nomeAtributo);
        return $this->$metodo();
    }
}