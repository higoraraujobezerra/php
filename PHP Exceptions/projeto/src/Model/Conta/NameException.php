<?php

namespace Alura\Banco\Model\Conta;

class NameException extends \DomainException
{
    public function __construct()
    {
        $mensagem = "O nome deve ter 5 ou mais caracteres.";
        parent::__construct($mensagem);
    }

}