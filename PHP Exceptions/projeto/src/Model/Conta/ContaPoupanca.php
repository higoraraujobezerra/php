<?php

namespace Alura\Banco\Model\Conta;

class ContaPoupanca extends Conta
{
    public function percentualTarifa():float
    {
        return 0.03;
    }
}