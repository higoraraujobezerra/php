<?php

namespace Alura\Banco\Model\Conta;

use Alura\Banco\Model\Conta\Titular;
use Alura\Banco\Model\Conta\SaldoInsuficienteException;

abstract class Conta
{
    private Titular $titular;
    protected float $saldo;
    private static $numeroDeContas = 0;

    public function __construct(Titular $titular)
    {
        $this->titular = $titular;
        $this->saldo = 0;

        self::$numeroDeContas++;
    }
    
    public function __destruct()
    {
        self::$numeroDeContas--;
    }

    public function sacar(float $valorASacar): void 
    {
        $valorASacar += $valorASacar * $this->percentualTarifa();
        if($valorASacar > $this ->saldo){
            throw new SaldoInsuficienteException($valorASacar, $this->saldo);
        }
        $this->saldo -= $valorASacar;
    }

    public function depositar(float $valorADepositar): void
    {
        if($valorADepositar < 0){
            throw new \InvalidArgumentException();        
        }

        $this->saldo += $valorADepositar;
    }

    public function recuperarSaldo(): float
    {
        return $this->saldo;
    }

    public function recuperarNomeTitular() : string
    {
        return $this->titular->recuperarNome();
    }

    public function recuperarCpfTitular() : string
    {
        return $this->titular->recuperarCpf();
    }


    public static function recuperarNumeroDeContas(): int
    {
        return self :: $numeroDeContas;
    }

    abstract function percentualTarifa():float;
 
}