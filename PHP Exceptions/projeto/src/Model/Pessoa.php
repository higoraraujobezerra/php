<?php

namespace Alura\Banco\Model;

use Alura\Banco\Model\Conta\NameException;
use Alura\Banco\Model\AcessoPropriedades;

abstract class Pessoa 
{
    use AcessoPropriedades;

    protected string $nome;
    private CPF $cpf;

    public function __construct(string $nome, CPF $cpf)
    {
        $this-> validaNome($nome);
        $this->nome = $nome;
        $this->cpf = $cpf;
    }

    public function recuperarNome() : string
    {
        return $this->nome;
    }

    public function recuperarCpf() : string
    {
        return $this->cpf->recuperarNumero();
    }

    final protected function validaNome(string $nome)
    {
        if(strlen($nome)<5){
            throw new NameException();
        }
    }
    
}