<?php

require_once 'autoload.php';

use Alura\Banco\Model\CPF;
use Alura\Banco\Service\Autenticador;
use Alura\Banco\Model\Funcionario\Diretor;
use Alura\Banco\Model\Funcionario\Gerente;

$umDiretor = new Diretor(
    'Higor',
    new CPF('123.456.789-10'),
    15000
);

$autenticador = new Autenticador();

$autenticador->tentaLogin($umDiretor, '1234');