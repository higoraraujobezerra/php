<?php

function funcao1()
{
    echo "Entrei na funcao 1." . PHP_EOL;

    //multi-catch
    try{

        funcao2();

    } //catch(RuntimeException | DivisionByZeroError $erroOuExecao){
      //catch(Exception $$erroOuExecao){
      //catch(Error $erroOuExecao){
      //catch(Exception | Execao $erroOuExecao){
      catch(Throwable $erroOuExecao){     
        echo $erroOuExecao->getMessage() . PHP_EOL;
        echo $erroOuExecao->getLine() . PHP_EOL;
        echo $erroOuExecao->getTraceAsString() . PHP_EOL;

        throw new RuntimeException(
            "Exceção foi tratada, mas, pega aí!",
            1,
            $erroOuExecao
        );
    }

    echo "Saindo da funcao 1" . PHP_EOL;
}

function funcao2()
{
    echo "Entrei na funcao2" . PHP_EOL;

    $exception = new RuntimeException();
    throw $exception;

    $arrayFixo = new SplFixedArray();
    $arrayFixo[3] = "Valor";

    for($i=1; $i <= 5; $i++){
        echo $i . PHP_EOL;
    }
}

echo "Iniciando o programa principal" . PHP_EOL;
funcao1();
echo "Finalizando o programa principal" . PHP_EOL;
