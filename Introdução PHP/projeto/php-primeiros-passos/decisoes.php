<?php


// OU = || and OR
// E = && and AND

$idade = 17;
$numeroDePessoas = 2;

echo "Você só pode entrar se tiver mais do que 18 anos.".PHP_EOL;

if($idade >= 18){
    echo "Você tem $idade anos. Pode entrar".PHP_EOL;
}else if($numeroDePessoas > 1){
    echo "Você tem $idade anos e está acompanhado. Pode entrar".PHP_EOL;
}else{
    echo "Você tem $idade anos. Não pode entrar".PHP_EOL;
}


//Operador ternario 
$variavel = $condicao ? $valorSeVerdadeiro : $valorSeFalso;
?>