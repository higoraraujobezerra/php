# PHP INTRODUCTION COURSE

### What's PHP?

    - Interpreted programming language - ""No" needs to be compiled;
    - general purpose language (for web, for desktop, for IOT, on command line etc);
    - The php -v command prints the installed PHP version;
    - The php -a command opens the interactive shell (CLI);
    - Reference: https://php.net/.

### Variable and types

    - PHP is a dynamically typed programming language;
    - The variable name must not use special characters;
    - The mathematical operators:
        Sum: +
        Subtraction: -
        Multiplication: *
        Division: /
        Rest of division (module): %
        Power: **
    - gettype command prints the type of a variable

### Text

    - To concatenate a string with another variable we use . (dot);
    - We can define a string with single(') or double(") quotes;
    - To interpret a variable value or special character within the string we must use double quotes;
    - New line: \n ou \r;
    - Tab: \t;
    - PHP has a constant to indicate a line break: PHP_EOL.


### Decisions
    - if
    - else
    - Logical operators:
        &&
        ||
        and
        or
    - Comparison Operators:
        <
        >
        ==
        !=
    
    - Ternary operator: 
        $variavel = $condicao ? $valorSeVerdadeiro : $valorSeFalso;

### Repetition
    - while
    - do-while
    - for
    - continue
    - break
