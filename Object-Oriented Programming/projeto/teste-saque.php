<?php

require_once 'autoload.php';

use Alura\Banco\Model\Conta\{Conta, ContaPoupanca, ContaCorrente, Titular};
use Alura\Banco\Model\{CPF, Endereco};

$conta = new ContaPoupanca(
    new Titular(
        new CPF('406.360.178-18'),
        'Higor', 
        new Endereco(
            'Sorocaba', 
            'Éden', 
            'rua', 
            '43'
        )
    )
);

$conta->depositar(500);
$conta->sacar(100);

echo $conta->recuperarSaldo();