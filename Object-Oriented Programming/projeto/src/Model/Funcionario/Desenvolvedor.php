<?php

namespace Alura\Banco\Model\Funcionario;

class Desenvolvedor extends Funcionario
{
    public function sobeDeNivel()
    {
        $this->recebeAumento($this->recuperarSalario() * 0.075);
    }

    public function calculaBonificacao(): float
    {
        return 500;
    }
}