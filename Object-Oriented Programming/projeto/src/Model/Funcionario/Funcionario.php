<?php

namespace Alura\Banco\Model\Funcionario;

use Alura\Banco\Model\{Pessoa, CPF};

abstract class Funcionario extends Pessoa
{
    private float $salario;

    public function __construct(string $nome, CPF $cpf, float $salario)
    {
        parent::__construct($nome, $cpf);
        $this->salario = $salario;
    }

    public function recuperarCargo() : string
    {
        return $this->cargo;
    }

    public function alterarNome(string $nome) : void
    {
        $this->validaNome($nome);
        $this->nome = $nome;
    }

    public function recebeAumento(float $valorAumento) : void
    {
        if($valorAumento < 0){
            echo "Aumento deve ser positivo";
            return;
        }

        $this->salario += $valorAumento;
    }

    public function recuperarSalario()
    {
        return $this->salario;
    }

    abstract public function calculaBonificacao():float;

}