<?php

require_once 'autoload.php';

use Alura\Banco\Model\Conta\Titular;
use Alura\Banco\Model\Endereco;
use Alura\Banco\Model\CPF;
use Alura\Banco\Model\Conta\Conta;



$primeiroEndereco = new Endereco('Sorocaba', 'Eden', 'Teste', '43');
$primeiroTitular = new Titular( new Cpf('406.360.178-18'), 'Higor Araujo', $primeiroEndereco);
$primeiraConta = new Conta($primeiroTitular);
var_dump($primeiraConta);

$primeiraConta->depositar(300);
//$primeiraConta->saldo -= 300;

echo $primeiraConta->recuperarSaldo() . PHP_EOL;

echo $primeiraConta->recuperarNomeTitular(). PHP_EOL;

echo $primeiraConta->recuperarCpfTitular() . PHP_EOL;

//$segundaConta = new Conta(new Titular('445123444564', 'Ana Paula'));

//new Conta(new Titular('445123444564', 'Ana Paula'));



