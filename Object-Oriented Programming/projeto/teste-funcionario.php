<?php

require_once 'autoload.php';

use Alura\Banco\Model\CPF;
use Alura\Banco\Model\Funcionario\{Gerente, Desenvolvedor, EditorVideo, Diretor};
use Alura\Banco\Service\ControladorDeBonificacao;

$umFuncionario = new Desenvolvedor(
    'Higor',
    new CPF('406.360.178-18'),
    1000.
);

$umGerente = new Gerente(
    'Karol',
    new CPF('441.234.544-61'),
    10000.
);

$umDiretor = new Diretor(
    'Karolina',
    new CPF('441.234.544-61'),
    5000.
);

$umEditorVideo = new EditorVideo(
    'Fernando',
    new CPF('441.234.544-61'),
    1500.
);

$controlador = new ControladorDeBonificacao();
$controlador->adicionaBonificacao($umFuncionario);
$controlador->adicionaBonificacao($umGerente);
$controlador->adicionaBonificacao($umDiretor);
$controlador->adicionaBonificacao($umEditorVideo);

echo $controlador->recuperarTotal();