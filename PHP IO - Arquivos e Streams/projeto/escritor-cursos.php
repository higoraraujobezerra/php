<?php 

/**
 * Primeira forma de escrever em um arquivo
 */

$arquivo = fopen('cursos-php.txt', 'a');

fwrite($arquivo, 'Design Patterns PHP II: Boas práticas de programação' . PHP_EOL);

fclose($arquivo);

/**
 * Segunda forma de escrever em um arquivo
 */

file_put_contents('cursos-php.txt', 'Design Patterns PHP I: Boas práticas de programação' . PHP_EOL, FILE_APPEND);
