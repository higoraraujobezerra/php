Curso Introdução ao PHP: primeiros passos com a linguagem
Curso PHP e MySQL: introdução a uma webapp
Curso Avançando com PHP: Arrays, Strings, Função e Web
Curso PHP Strings: Operações e expressões regulares
Curso PHP: manipulando coleções com Arrays
Curso PHP: dominando as Collections
Curso Orientação a Objetos com PHP: Classes, métodos e atributos
Curso Avançando com Orientação a Objetos com PHP: Herança, Polimorfismo e Interfaces
Curso PHP Brasil: validação de dados no padrão nacional
Curso PHP Composer: Dependências, Autoload e Publicação
Curso PHP Exceptions: tratamento de erros
Curso PHP I/O: trabalhando com arquivos e streams
Curso PHP e Behavior Driven Development: BDD com Behat
Curso PHP Parallel: Processos, Threads e Channels
Curso Object Calisthenics: exercitando a Orientação a Objetos
Curso Metaprogramação com PHP: API de Reflection
Curso PHP e Clean Architecture: descomplicando arquitetura de software