<?php

/**
* Primeira forma de ler um arquivo
*/

$arquivo = fopen('lista-cursos.txt', 'r');

while (!feof($arquivo)) {
    $curso = fgets($arquivo);

    echo $curso;
}

fclose($arquivo);

echo PHP_EOL . '-------------------------------------------------------------------------------' . PHP_EOL;

/*
* Segunda forma de ler um arquivo
*/

$arquivo = fopen('lista-cursos.txt', 'r');

$tamanhoArquivo = filesize('lista-cursos.txt');

$cursos = fread($arquivo, $tamanhoArquivo);

echo $cursos;

fclose($arquivo);

echo PHP_EOL . '-------------------------------------------------------------------------------' . PHP_EOL;

/**
* Terceira forma de ler um arquivo
*/

$cursos = file_get_contents('lista-cursos.txt');

echo $cursos;

echo PHP_EOL . '-------------------------------------------------------------------------------' . PHP_EOL;

/**
* Quarta forma de ler um arquivo
*/

$cursos = file('lista-cursos.txt');

var_dump($cursos);
