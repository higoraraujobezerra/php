# MOVING FORWARD WITH PHP

### Array

    - Array or vector or list;
    - DDeclare array: [] or array();
    - To find out how many elements an array has there is the count() function.
    - In simple arrays with numeric index, just use [] to add an element at the end (eg $list[] = 12;)
      in that case PHP automatically increments the index;
    - To remove use unset(..);


### Array associativos

    - https://www.php.net/manual/pt_BR/language.types.array.php
    - An associative array always defines elements composed of a key and a value.
        key => value
        The key must be of type integer or string, all other types PHP tries to convert
    - An associative array is also called a map or dictionary.
    - The foreach loop offers a more expressive form of iteration;
    - Take several elements at once:
        $idadeList = [21, 23, 19, 25, 30, 41, 18] 
        list($idadeVinicius, $idadeJoao, $idadeMaria) = $idadeList;
    
### Function

    - Function or subrotine?
        Calling a function in code now represents the value it returns. With that, we can assign the return of a
        function to a variable, for example, or even display it. A subroutine, on the other hand, only executes a code in isolation, 
        without returning no value.
    - A function encapsulates a functionality/code;
    - a function can specify the types in the parameter declaration (as of version 7) as well as the return type.

### Interpolation and require

    https://www.php.net/manual/en/language.types.string.php
    https://www.php.net/manual/en/errorfunc.constants.php

    - Associative array within string:
        $conta[titular] - No quotes inside the [];
        {$conta['titular']} - Com {};
    - Include does not give an error (just warns) if the file does not exist;
    - Require gives error;
    - Require_once guarantees that the file will only be included once;
    - E_NOTICE, PHP gives a warning but "turns around" and continues with execution;
    - E_ERROR, PHP gives error and program execution stops.

### References and Lists

    When you pass a para parameter to a function, the function understands that parameter as a copy of the original. If you use the &,
    (eg: &$account) is passed the original value.

    - Value or by reference: 
        Value - PHP makes a copy of the parameter;
        Reference - PHP accesses the value of the original variable. To use reference passing you must use & in front of the variable.

### Introdução do PHP a WEB

    - Local server
        comando php -S localhost:8080
    - Browser - CTRL + U = source code
    - HTTP (Hyper Text Transfer Protocol)
    
