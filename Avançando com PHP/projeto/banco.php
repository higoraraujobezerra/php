<?php
//include 'functions.php';
//or
//require 'functions.php';
//or
require_once 'functions.php';

$contasCorrente = [
    12345678910 => [
        'nome' => 'Higor',
        'saldo' => '1000'
    ],
    12345678911 => [
        'nome' => 'Maria',
        'saldo' => '10000'
    ], 
    12345678912 => [
        'nome' => 'Lurdes',
        'saldo' => '1000']
];

$contasCorrente[12345678910]= sacar($contasCorrente[12345678910], 500);

$contasCorrente[12345678910]= depositar($contasCorrente[12345678910], 1000);

//titularComLetrasMaiusculas($contasCorrente[12345678910]);

unset($contasCorrente[12345678910]);

echo "<ul>";

foreach($contasCorrente as $cpf => $conta){

    // exibeMensagem(
    //     "{$cpf} {$conta['nome']} {$conta['saldo']}"
    // );

    // echo "\n";

    // ['nome' => $titular, 'saldo' => $saldo] = $conta;
    // exibeMensagem(
    //     "$cpf $titular $saldo"
    // );

    //echo "\n";

    //exibeConta($conta);
}

echo "</ul>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Contas corrente</h1>
    <dl>
        <?php foreach($contasCorrente as $cpf =>$conta){?>
        <dt>
            <h3> <?=$conta['nome'];?> - <?=$cpf;?></h3>
        </dt>
        <dd>
            Saldo: <?=$conta['saldo'];?>
        </dd>
        <?php } ?>
    </d1>
</body>
</html>
