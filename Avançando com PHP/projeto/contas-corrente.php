<?php

$conta1 = [
    'nome' => 'Higor',
    'saldo' => '1000'
];

$conta2 = [
    'nome' => 'Maria',
    'saldo' => '10000'
];

$conta3 = [
    'nome' => 'Alberto',
    'saldo' => '300'
];

$contasCorrente = [$conta1, $conta2, $conta3];

for($i=0; $i < count($contasCorrente); $i++){
    echo $contasCorrente[$i]['nome'] . PHP_EOL;
}


