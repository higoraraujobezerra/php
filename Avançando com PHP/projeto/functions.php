<?php

function exibeMensagem($mensagem){
    echo $mensagem . '<br>'. PHP_EOL ;
}

function sacar(array $conta, float $saque): array
{

    if($saque > $conta['saldo']){
        exibeMensagem("Você não pode sacar");
    }else{
        $conta['saldo'] -= $saque;
    }

    return $conta;
}

function depositar(array $conta, float $deposito)
{

    if($deposito > 0){
        $conta['saldo'] += $deposito;
    }else{
        exibeMensagem("Você não pode depositar esse valor!");
    }

    return $conta;
}

function titularComLetrasMaiusculas(array &$conta)
{
    $conta['nome'] = strtoupper($conta['nome']);

    exibeConta($conta);
}

function exibeConta(array $conta)
{

    ['titular' => $titular, 'saldo' => $saldo] = $conta;
    echo "<li> Titular:$titular Saldo: $saldo </li>";
}