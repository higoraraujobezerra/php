<?php

$notaPortugues = 9;
$notaMatematica = 3;
$notaGeografia = 10;
$notaHistoria = 5;

echo "<p> a nota de português é: {$notaPortugues} </p>" . PHP_EOL;
echo "<p> a nota de matemática é: {$notaMatematica} </p>" . PHP_EOL;
echo "<p> a nota de geografia é: {$notaGeografia} </p>" . PHP_EOL;
echo "<p> a nota de história é: {$notaHistoria} </p>" . PHP_EOL;

$media = ($notaMatematica + $notaHistoria + $notaGeografia + $notaPortugues)/4;

echo "<p> a média é: {$media} </p>";