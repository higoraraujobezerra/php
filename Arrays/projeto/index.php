<?php

namespace Alura;

require "autoload.php";

$correntista = [
    "Giovani",
    "João",
    "Maria",
    "Luis",
    "Luisa",
    "Rafael"
];

$saldos = [
    2500,
    3000,
    4400,
    1000,
    8700,
    9000
];

$relacioados = array_combine($correntista, $saldos);

$maiores = ArrayUtils::encontrarPessoasComSaldoMaior(3000, $relacioados);

echo "<pre>";
var_dump($maiores);
echo"</pre>";

