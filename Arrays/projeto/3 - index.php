<?php declare(strict_types=1);

namespace Alura;

require_once "autoload.php";

$notas = [9, 3, 10, 5, 10];

$calculadora = new Calculadora;

$media = $calculadora->calculaMedia($notas);

if($media){
    echo " <p> A média é: {$media} </p>";
}else{
    echo "<p> Não foi possível calcular a média. </p>";
}

echo "<pre>";

var_dump($notas);

ArrayUtils::remover("3", $notas);

var_dump($notas);

echo "</pre>";