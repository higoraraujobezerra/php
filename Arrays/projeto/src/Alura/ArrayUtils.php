<?php declare(strict_types=1);

namespace Alura;

class ArrayUtils 
{
    public static function remover(string $elemento, array &$array)
    {
        $posicao = array_search($elemento, $array, $strict = true);
        var_dump($posicao);
        if(is_int($posicao)){
            unset($array[$posicao]);
        }
    }

    public static function encontrarPessoasComSaldoMaior(int $saldo, $array) : array
    {
        $correntistaComSaldoMaior = [];
        foreach($array as $chave => $valor){

            if($valor > $saldo){
                $correntistaComSaldoMaior[] = $chave;
            }
        }

        return $correntistaComSaldoMaior;
    }
}