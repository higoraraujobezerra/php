<?php

namespace Alura;

class Calculadora
{
    public function calculaMedia(array $notas) : ?float
    {
        $quantidaNotas = sizeof($notas);

        if($quantidaNotas > 0){
            $soma = 0;

            foreach($notas as $n) 
            {
                $soma += $n;
            }
    
            $media = $soma / $quantidaNotas;
    
            return $media;
        }else{
            return null;
        }
    }
}